"use strict"

module.exports = function(sequelize, DataTypes) {
	var AuditLog = sequelize.define("AuditLog", {
		userId: {
			type: DataTypes.STRING,
			allowNull: false
		},
		type: {
			type: DataTypes.STRING,
			allowNull: false
		},
		namespace: {
			type: DataTypes.STRING,
			allowNull: false
		},
		resource : {
			type: DataTypes.STRING,
			allowNull: false
		},
		resourceId: {
			type: DataTypes.STRING,
			allowNull: false
		},
		time : {
			type: DataTypes.DATE,
			allowNull: false
		},
		meta: DataTypes.JSONB
	}, {
		tableName: 'AuditLog'
	});

	AuditLog.associate = function(db) {
	}

	AuditLog.getForOrg = function(namespace, query, limit, offset){
		if(!query) query = {}

		query.namespace = namespace

		return AuditLog.findAll({
			where: query,
			order: [
				['updatedAt']
			],
			limit: limit,
			offset: offset
		});
	}

	return AuditLog;
}