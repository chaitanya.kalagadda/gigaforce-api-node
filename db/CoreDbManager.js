var db = require("./index");

function getDb() {
    return db.getDb('core');
}

function getModel(modelName) {
    return getDb().then(coreDb => {
        return coreDb[modelName];
    })
}

function dropDatabase() {
    return db.deleteDB('core');
}

module.exports = {
    getDb: getDb,
    getModel: getModel,
    dropDB: dropDatabase
}
