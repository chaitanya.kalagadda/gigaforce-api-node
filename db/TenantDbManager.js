var db = require('./index');
var debug = require('debug')('tenant-db-manager');

function getDb (tenantId) {
  var dbName = 'tdb_' + tenantId.replace(/-/g, '_');
  return db.getTenantDb(dbName);
}

function getModel (tenantId, modelName) {
  return getDb(tenantId).then(tenantDb => {
    return tenantDb[modelName];
  });
}

module.exports = {
  getModel: getModel,
  getDb   : getDb
};
