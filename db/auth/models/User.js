"use strict";

module.exports = function(sequelize, DataTypes) {
	const Op = sequelize.Op;

	var User = sequelize.define("User", {
		id: {
			type: DataTypes.UUID,
			allowNull: false,
			primaryKey: true,
			defaultValue: DataTypes.UUIDV4
		},
		firstName: {
			type: DataTypes.STRING
		},
		lastName: {
			type: DataTypes.STRING
		},
		emailId: {
			type: DataTypes.STRING,
			allowNull: false,
			validate: {
				isEmail: true
			}
		},
		hash: {
			type: DataTypes.TEXT,
			allowNull: false
		},
		meta: DataTypes.JSONB,
	}, {
		tableName: 'User'
	});

	User.associate = function(db) {
		
	};

	User.findByEmail = function(emailId) {
		return User.findOne({
			where: {
				emailId: emailId
			}
		});
	};
	
	User.getById = function(id) {
		return User.findOne({
			where: {
				id: id
			}
		});
	};
	
	User.getByIds = function(ids, attributes) {
		let query = {
			where: {
				id: {
					[Op.or]: ids
				}
			}
		};

		if(attributes) query['attributes'] = attributes;
		return User.findAll(query);
	};

	User.getSuperAdmins = function() {
		return User.findAll({
			where: {
				meta: {
					isSuperAdmin: true
				}
			},
			attributes: [
				'id',
				'emailId'
			]
		});
	};

	return User;
};
