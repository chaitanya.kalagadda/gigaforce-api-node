
module.exports = function (sequelize, DataTypes) {
  const Op = sequelize.Op;

  var App = sequelize.define("App", {
    id: {
      type        : DataTypes.UUID,
      allowNull   : false,
      primaryKey  : true,
      defaultValue: DataTypes.UUIDV4
    },
    status: {
      type        : DataTypes.STRING,
      allowNull   : false,
      defaultValue: 'active' // active, in-active, pending-payment
    },
    name: {
      type     : DataTypes.STRING,
      allowNull: false,
      unique   : 'app_name'
    },
    botId: {
      type     : DataTypes.UUID,
      allowNull: false,
      field    : 'bot_id',
      unique   : 'app_name'
    },
    orgId: {
      type     : DataTypes.UUID,
      allowNull: false,
      field    : 'org_id'
    },
    description: {
      type     : DataTypes.TEXT('medium'),
      allowNull: true
    },
    meta: {
      type     : DataTypes.JSONB,
      allowNull: true
    },
    objectIds: {
      type        : DataTypes.ARRAY(DataTypes.UUID),
      allowNull   : true,
      defaultValue: [],
      field       : 'object_ids'
    }
  }, {
    tableName: 'app',
    paranoid : true,
    indexs   : [
      {
        name  : 'app_name_idx',
        fields: ['name']
      }, {
        name  : 'app_bot_id_idx',
        fields: ['bot_id']
      }, {
        name  : 'app_org_id_idx',
        fields: ['org_id']
      }
    ]
  });

  App.associate = function (db) {
    App.belongsTo(db.Bot, {
      as        : 'Bot',
      foreignKey: {
        allowNull: false
      },
      onDelete: 'cascade'
    });
    App.hasMany(db.AppUserRoles, { foreignKey: 'appId' });

  };

  App.findByAppId = function (appId, raw) {
    let query = {
      where: {
        id: appId
      }
    };
    if (raw) {
      query['raw'] = true;
    }
    return App.findOne(query);
  };

  App.updateObjects = function (appId, objects) {
    return App.update(
      {
        objectIds: objects
      }, {
        where: {
          id: appId
        }
      }
    );
  };

  App.getObjectIds = function (appId) {
    return App.findOne({
      attributes: ['objectIds'],
      where     : {
        id: appId
      }
    });
  };

  App.getAppByName = function (orgId, botId, name) {
    return App.findOne({
      where: {
        orgId,
        botId,
        name
      }
    });
  };

  App.createApp = function (data) {
    return App.create(data);
  };

  App.updateAppById = function (appId, data) {
    return App.update(data, {
      where: {
        id: appId
      }
    });
  };

  App.deleteApp = function (appId) {
    return App.destroy({
      where: {
        id: appId
      },
      force: true
    });
  };

  App.getAppsByBotId = function (botId) {
    return App.findAll({
      where: {
        botId
      },
      raw: true
    });
  };

  App.removeDatasheet = function (botId, datasheetId) {
    var qr = "UPDATE \"app\" SET \"object_ids\" = array_remove(\"object_ids\", $value) WHERE \"BotId\" = $botId;";
    return sequelize.query(qr, {
      bind: {
        value: datasheetId,
        botId: botId
      },
      type: sequelize.QueryTypes.UPDATE
    });
  };

  return App;

};
