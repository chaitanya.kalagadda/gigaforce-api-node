const Promise = require('bluebird');

function getIndexQueries (version) {
  var CREATE_INDEX_QUERIES_LIST = [
    'CREATE INDEX IF NOT EXISTS message_sentMessage_idx ON "Messages" ("sentMessage");',
  ];

  var CREATE_INDEX_QUERIES = CREATE_INDEX_QUERIES_LIST.join('');
  return CREATE_INDEX_QUERIES;
}

const DROP_INDEX_QUERIES_LIST = [
  'DROP INDEX IF EXISTS message_sentMessage_idx;'
];

var DROP_INDEX_QUERIES = DROP_INDEX_QUERIES_LIST.join('');

function up (query, DataTypes, version) {
  return query.sequelize.query(getIndexQueries(version), { raw: true });
}

function down (query, DataTypes) {
  return query.sequelize.query(DROP_INDEX_QUERIES, { raw: true });
}

module.exports = {
  up  : up,
  down: down
};
