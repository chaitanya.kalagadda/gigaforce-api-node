var db = require('./index');

function getUserModel() {
	return db.getAuthDb().then(authDb => {
		return authDb.User;
	})
}

function getDb() {
	return db.getAuthDb();
}

function getModel(modelName) {
	return getDb().then(db => {
		return db[modelName]
	})
}

function getBotInfoModel() {
	return getModel('BotInfo');
}

function getOrgModel() {
	return getModel('Organization');
}

module.exports = {
	getUserModel : getUserModel,
	getDb: getDb,
	getBotInfoModel: getBotInfoModel,
	getOrgModel: getOrgModel
}