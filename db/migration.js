const Umzug = require('umzug');
var debug = require('debug')('migration');

function perfromMigrations (sequelize, migrationsPath, dbVersion) {
  const umzug = new Umzug({
    storage       : 'sequelize',
    storageOptions: {
      sequelize: sequelize,
    },

    // see: https://github.com/sequelize/umzug/issues/17
    migrations: {
      params: [
        sequelize.getQueryInterface(), // queryInterface
        sequelize.constructor, // DataTypes
        dbVersion,
        function () {
          throw new Error('Migration tried to use old style "done" callback. Please upgrade to "umzug" and return a promise instead.');
        }
      ],
      path   : migrationsPath,
      pattern: /\.js$/
    },

    logging: function () {
      debug.apply(null, arguments);
    },
  });

  function logUmzugEvent (eventName) {
    return function (name, migration) {
      debug(`${ name } ${ eventName }`);
    };
  }
  umzug.on('migrating', logUmzugEvent('migrating'));
  umzug.on('migrated', logUmzugEvent('migrated'));
  umzug.on('reverting', logUmzugEvent('reverting'));
  umzug.on('reverted', logUmzugEvent('reverted'));

  return umzug.up();
}

module.exports = perfromMigrations;
