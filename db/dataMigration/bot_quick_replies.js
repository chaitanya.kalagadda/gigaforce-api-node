var _ = require('lodash');
var TenantDbManager = require('../TenantDbManager');
var AuthDbManager = require('../AuthDbManager');
var debug = require('debug')('bot-quick-replies');
var async = require('async');

const CHIPS_STYLE = {
  backgroundColor       : 'rgba(255, 255, 255)',
  textColor             : '#000000ff',
  chipWidth             : "variable",
  borderRadius          : "16",
  margin                : 2,
  border                : 1,
  label                 : 'icon',
  textAlign             : 'left',
  fontSize              : 12,
  bold                  : false,
  italic                : false,
  underline             : false,
  enabled               : true,
  height                : 24,
  noOFQuickRepliesToShow: 5,
  borderColor           : '#000000ff',
  hoverColor            : "#efefefff"
};

const BUBBLES_STYLE = {
  backgroundColor       : "#ffffff00",
  textColor             : '#000000ff',
  chipWidth             : 'full',
  borderRadius          : "0",
  margin                : 1,
  border                : 0,
  label                 : 'bullet',
  textAlign             : 'left',
  fontSize              : 12,
  bold                  : true,
  italic                : false,
  underline             : false,
  enabled               : true,
  height                : 24,
  noOFQuickRepliesToShow: 5,
  borderColor           : "#000000ff",
  hoverColor            : "#efefefff"
};

const DEFAULT_STYLE = {
  backgroundColor       : "#ffffffff",
  textColor             : '#000000ff',
  chipWidth             : "variable",
  borderRadius          : "0",
  margin                : 2,
  border                : 1,
  label                 : 'icon',
  textAlign             : 'left',
  fontSize              : 12,
  bold                  : false,
  italic                : false,
  underline             : false,
  enabled               : true,
  height                : 24,
  noOFQuickRepliesToShow: 5,
  borderColor           : "#000000ff",
  hoverColor            : "#efefefff"
};


function updateFromExistingTheme (style, settings) {

  var textColor = _.get(settings, 'theme.quickReplies.color');
  if (textColor) {
    style.textColor = textColor;
  }

  var borderColor = _.get(settings, 'theme.quickReplies.backgroundColor');
  if (textColor) {
    style.borderColor = borderColor;
  }

  var fontSize = Number(_.get(settings, 'theme.quickReplies.fontSize'));
  if (fontSize) {
    style.fontSize = fontSize;
  }

  return style;
}
async function migrate (orgId) {
  var db = await TenantDbManager.getDb(orgId);
  var botsList = await db.Bot.findAll();

  var updatePromises = _.map(botsList, function (bot) {
    var settings = _.get(bot, 'settings');
    if (_.has(settings, 'theme')) {
      var style;
      var qrStyle = _.get(settings, 'theme.quickReplies.style');
      if (qrStyle === 'chips') {
        style = _.clone(CHIPS_STYLE);
      } else if (qrStyle === 'bubbles') {
        style = _.cloneDeep(BUBBLES_STYLE);
      } else {
        style = _.clone(DEFAULT_STYLE);
      }

      updateFromExistingTheme(style, settings);
      _.set(settings, 'theme.quickRepliesStyle', style);
      debug('updating theme with quick reply styles %j, for bot %s', settings.theme, bot.id);
      return bot.update({settings: settings})
        .catch(err => {
          debug('error while updating bot with id ', bot.id);
          debug(err);
        });
    }
  });

  var result = await Promise.all(updatePromises);
  await db.sequelize.close();

}

async function bot_quick_replies_migration () {
  var AuthDb = await AuthDbManager.getDb();
  var botinfolist = await AuthDb.BotInfo.getAllBotsInfo();
  var orgsList = await AuthDb.Organization.findAll();
  async.eachSeries(orgsList, function (org, callback) {
    var orgId = org.id;
    debug("migrating quick replies data for OrgId: "+ orgId);
    return migrate(orgId)
      .catch(err => {
        debug(err);
      })
      .then(() => {
        callback();
      });
  }, (err) => {
    if (err) {
      debug(err);
    }
    debug('===================== bot quick replies migration complete =====================\n\n\n');
    process.exit();
  });
}


bot_quick_replies_migration();
