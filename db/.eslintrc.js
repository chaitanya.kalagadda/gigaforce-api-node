module.exports = {
  "rules": {
    "no-unused-vars": ["warn", {"varsIgnorePattern": "(DataTypes|Promise|debug)"}],
  }
};
