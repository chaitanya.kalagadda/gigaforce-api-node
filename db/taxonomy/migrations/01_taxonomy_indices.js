const Promise = require('bluebird');

function getIndexQueries(version) {
	var ops = version >= 100000 ? 'array_ops' : '_varchar_ops';
	const CREATE_INDEX_QUERIES_LIST = [
	'CREATE INDEX IF NOT EXISTS taxonomy_token_idx ON "Taxonomy" USING GIN (token ' + ops + ');',
	'CREATE INDEX IF NOT EXISTS taxonomy_type_idx ON "Taxonomy" (type);'
	]

	var CREATE_INDEX_QUERIES = CREATE_INDEX_QUERIES_LIST.join('');
	return CREATE_INDEX_QUERIES;
}

const DROP_INDEX_QUERIES_LIST = [
'DROP INDEX IF EXISTS intent_event_name_idx;',
'DROP INDEX IF EXISTS taxonomy_type_idx;',
]

var DROP_INDEX_QUERIES = DROP_INDEX_QUERIES_LIST.join('');

function up(query, DataTypes, version) {
	return query.sequelize.query(getIndexQueries(version), { raw: true });
}

function down(query, DataTypes) {
    return query.sequelize.query(DROP_INDEX_QUERIES, { raw: true });
}

module.exports = {
    up: up,
    down: down
};