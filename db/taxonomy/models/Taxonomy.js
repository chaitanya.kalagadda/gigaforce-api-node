"use strict"

var config = require('../../../config');
var defaultThreshold = config.get('bot.ml.threshold');
var defaultLanguages = config.get('bot.ml.defaultLanguages');
var enablePlatformResponse = config.get('bot.ml.enablePlatformResponse');
var _ = require('lodash');

module.exports = function(sequelize, DataTypes) {
	const Op = sequelize.Op

	var Taxonomy = sequelize.define("Taxonomy", {
		id: {
			type: DataTypes.UUID,
			allowNull: false,
			primaryKey: true,
			defaultValue: DataTypes.UUIDV4
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false
		},
		type: {
			type: DataTypes.STRING,
			allowNull: false
		},
		displayName: {
			type: DataTypes.STRING,
			allowNull: false
		},
		token: {
      type: DataTypes.ARRAY(DataTypes.STRING),
      allowNull: false
    },
		intentsAssociated: {
      type: DataTypes.ARRAY(DataTypes.STRING),
    },
		mappedCategories: {
      type: DataTypes.ARRAY(DataTypes.STRING),
    },
    entries: {
			type: DataTypes.ARRAY(DataTypes.JSONB),
    },
		metadata: {
      type: DataTypes.JSONB,
    }
	}, {
		tableName: 'Taxonomy'
	});

	Taxonomy.getByIds = function(ids){
		if(!_.isEmpty(ids)){
			return Taxonomy.findAll({
				where: {
						id:{
							[Op.or]:ids
					}
				}
			})
		} else {
			return Promise.resolve();
		}
	}

	Taxonomy.getNamesForIds = function(ids){
		if(!_.isEmpty(ids)){
			return Taxonomy.findAll({
				where: {
						id:{
							[Op.or]:ids
					}
				},
				attributes: ['id', 'name', 'displayName']
			})
		} else {
			return Promise.resolve();
		}
	}

	Taxonomy.getByDisplayNames = function(displayName){
		if(!_.isEmpty(displayName)){
			return Taxonomy.findAll({
				where: {
					displayName:{
							[Op.or]:displayName
					}
				},
				attributes: ['id', 'name', 'displayName']
			})
		} else {
			return Promise.resolve();
		}
	}

	Taxonomy.getCategoryMatchingRecord = function(category){
		return Taxonomy.findOne({
			where: {
				name:{
					[Op.like] : '%'+category+'%',
				}
			}
		})
	}

	Taxonomy.getCategoryMappings = function(category){
		return Taxonomy.findAll({
			where: {
				name:{
					[Op.like] : '%'+category+'%',
				}
			}
		})
	}

	Taxonomy.getMappedCategoryForAtrribute = function(key, category){
		return Taxonomy.findAll({
			where: {
				type: "attribute",
				name:	key,
				mappedCategories:{
					[Op.contains]:category
				}
			}
		})
	}
	Taxonomy.getCategoryType = function(category){
		return Taxonomy.findAll({
			where: {
				type: "category",
				name:{
					[Op.or] : category,
				}
			}
		})
	}

	Taxonomy.getAttributeMappings = function(attributename){
		return Taxonomy.findAll({
			where: {
				type: "attribute",
				displayName:{
					[Op.like] : '%'+attributename,
				}
			}
		})
	}
	Taxonomy.getAttributeMappingsForAttrandCategory = function(attr_array,category, tokens){
		return Taxonomy.findAll({
			where: {
				[Op.and]:{
					name:{
						[Op.in]:attr_array
					},
					type: "attribute",
					mappedCategories:{
						[Op.contains]:[category]
					},
					token: {
						[Op.contains]: tokens
					}
				}
			}
		})
	}

	Taxonomy.getKeywordsforMultipleCategories = function(listofcategories){
		return Taxonomy.findAll({
			where: {
					name:{
						[Op.or]:listofcategories
				}
			}
		})
	}

	Taxonomy.getMatchingRecordsforKeywords = function(keywords){
		var qr = "SELECT DISTINCT x.ng, tx.name,x.tk as matched_tokens, * FROM \"Taxonomy\" AS \"tx\" ,\
							LATERAL (SELECT tk, (length(tk) - length(replace(tk, ' ', '')) + 1) as ng from unnest(tx.token) tk \
							where tk = ANY($tokens::text[])) x \
							ORDER by x.ng desc;"

		return sequelize.query(qr, {
			bind : {
				tokens: keywords,
			},
			type: sequelize.QueryTypes.SELECT
		})
	}

	Taxonomy.getByNameAndType = function(name, type){
		return Taxonomy.findOne({
			where: {
				displayName: name,
				type: type
			}
		});
	}

	Taxonomy.getListWithNameAndId = function() {
		return Taxonomy.findAll({
			attributes: ['id', ['displayName', 'name']]
		})
	}

	return Taxonomy;
}
