var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var config = require('../config');
var migration = require('./migration');
var Promise = require('bluebird');
var pg = require('pg');
var debug = require('debug')('db');
const { Client } = require('pg');

// This line is to make sure that values of count queries in postgres are parsed to number insted of string
pg.defaults.parseInt8 = true;

var dbs = {};

var username = config.get('db.username') || 'postgres';
var password = config.get('db.password') || 'postgres';
var host = config.get('db.host') || localhost + ':' + config.get('db.port') || 5431;

function checkAndCreateDBIfNotExists (dbName) {
  var conStringPri = 'postgres://' + username + ':' + password + '@' + host +
	'/postgres';

  return new Promise( (resolve, reject) => {
    const client = new Client({connectionString: conStringPri});
    client.connect(function (err, client, done) { // connect to postgres db
      if (err) {
        debug('Error while connecting: ' + err);
        return reject(err);
      }

      // Connection to postgres db successful try connecting the db with given name
      client.query('CREATE DATABASE ' + dbName, function (err) { // create user's db
        if (err) {
          debug('ignoring the error : ' + err); // ignore if the db is there
        } else {
          // DB Creation successful
          debug('created database successful');
        }

        client.end(); // close the connection
        resolve();
      });
    });
  });
}

function initSequelize (name) {
  var sequelize;
  var options = config.get('db.options');
  if (!options.host) {
    options.host = config.get('db.host') || 'localhost';
  }
  if (!options.port) {
    options.port = config.get('db.port') || 5431;
  }
  options.benchmark = true;
  options.logging = function (q, ts) {
    debug('%s Elapsed time is %s ms', q, ts);
  };
  options.pool = {
    acquire          : config.get('db.options.acquire'),
    handleDisconnects: true,
    max              : config.get('db.options.max'),
    idle             : config.get('db.options.idle')
  };
  options.pool.appName = "api";
  if (config.has('db.options.appName')) {
    options.pool.appName = config.get('db.options.appName');
  }
  if (config.get('db.replication.enable')) {
    debug('replication enabled, using replication config');
    options.replication = {
      read : config.get('db.replication.read'),
      write: config.get('db.replication.write')
    };
  }
  sequelize = new Sequelize(name, config.get('db.username'), config.get('db.password'), options);
  return sequelize;
}

function loadModels (db, sequelize, name) {
  debug('loading models for name: ', name);
  var modelsPath = path.join(__dirname, name, 'models');

  fs
    .readdirSync(modelsPath)
    .filter(function (file) {
      return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(function (file) {
      debug('loading model: %s', file);
      var model = sequelize["import"](path.join(modelsPath, file));
      db[model.name] = model;
    });

  Object.keys(db).forEach(function (modelName) {
    if ("associate" in db[modelName]) {
      db[modelName].associate(db);
    }

    var model = db[modelName];

    // add delete function to all
    model.delete = function (modelInst) {
      return modelInst.destroy().return(modelInst);
    };

    model.prototype.delete = function () {
      return this.destroy().return(this);
    };

    model.insert = function (data) {
      return db[modelName].build(data).save();
    };

    model.getById = function (id) {
      return db[modelName].findById(id);
    };
  });
}

function initInternal (name, isTenant) {
  var db = {};

  var sequelize = initSequelize(name);
  loadModels(db, sequelize, (isTenant ? 'tenant' : name));

  db.sequelize = sequelize;
  db.Sequelize = sequelize;

  return db;
}


function getDb (name, isTenant) {
  if (!dbs[name]) {
    var db = initInternal(name, isTenant);
    var dbFolderPath = path.join(__dirname, isTenant ? 'tenant' : name);
    dbs[name] = db;
    // TODO create db if it doesn't exist
    return checkAndCreateDBIfNotExists(name)
      .then(val => {
        return db.sequelize.sync({force: config.get('db.dropTables')});
      })
      .then(() => {
        return db.sequelize.query('show server_version_num', {raw: true})
          .then(verInfo => {
            return verInfo[0]['server_version_num'];
          });
      })
      .then(function (version) {
        db.version = version;
        var migrationsPath = path.join(dbFolderPath, 'migrations');
        if (fs.existsSync(migrationsPath)) {
          return migration(db.sequelize, migrationsPath, version).return(db);
        } else {
          debug('no migrations present for the db: %s', name);
          return db;
        }
      });
  } else {
    return Promise.resolve(dbs[name]);
  }
}

function getAuthDb () {
  return getDb('auth');
}

function getTenantDb (name) {
  return getDb(name, true);
}

function getAuthDbModel (modelName) {
  return getAuthDb()
    .then( authDb => {
      return authDb[modelName];
    });
}

function getTenantDbModel (tenantId, modelName) {
  return getTenantDb()
    .then( tenantDb => {
      return tenantDb[modelName];
    });
}

function getSessionDb () {
  if (!dbs['session']) {

    var db = {};

    var sequelize = initSequelize('session');

    db.sequelize = sequelize;
    db.Sequelize = sequelize;

    dbs['session'] = db;
    return db;
  } else {
    return dbs['session'];
  }
}

function initSessionDb () {
  return checkAndCreateDBIfNotExists('session')
    .then(getSessionDb);
}

module.exports = {
  getDb        : getDb,
  getAuthDb    : getAuthDb,
  getTenantDb  : getTenantDb,
  getSessionDb : getSessionDb,
  initSessionDb: initSessionDb,
  initAuthDb   : getAuthDb
};
