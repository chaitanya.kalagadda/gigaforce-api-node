var db = require('./index');

function getDb() {
	return db.getDb('auditlog');
}

function getModel(modelName) {
	return getDb().then(db => {
		return db[modelName]
	})
}

function getAuditLogModel() {
	return getModel('AuditLog');
}

module.exports = {
	getDb: getDb,
	getAuditLogModel: getAuditLogModel
}