var convict = require('convict');
var path = require('path');
var cjson = require('cjson');
const convict_format_with_validator = require('convict-format-with-validator');
// Use this only if you use the "email", "ipaddress" or "url" format
convict.addFormats(convict_format_with_validator);

// Define a schema
var config = convict(cjson.load(path.join(__dirname, 'schemas')));

// Load environment dependent configuration
var env = config.get('app.env');
config.loadFile('./config/' + env + '.json');

//set debug variables, ENV variables have highest priority
if (config.get('app.debug')) {
  if (process.env.DEBUG) {
    config.set('app.debug', process.env.DEBUG);
  } else {
    process.env.DEBUG = config.get('app.debug');
  }

  if (config.get('app.env') === 'prod') {
    console.warn('WARNING: DEBUG HAS BEEN ENABLED IN PRODUCTION');
  }
}
// Perform validation
config.validate();

module.exports = config;
